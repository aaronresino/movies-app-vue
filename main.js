var app = new Vue({
    el: '#app',
    data() {
        return {
            title: 'Movies DB',
            movies: []
        }
    },
    components: {
        Movies
    },
    mounted() {
        this.getPopularMovies();
    },
    methods: {
        getPopularMovies () {
            const urlRequest = `${ URL }/discover/movie?api_key=${ APIKEY_MOVIEDB }`;

            fetch(urlRequest)
            .then(response => response.json())
            .then(({ results }) => {
                this.movies = results;
                console.log(this.movies);
            });
        }
    },
});
