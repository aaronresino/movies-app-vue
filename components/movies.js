let Movies = Vue.component('Movies', {
    template:
    `<div class="movies-list">
        <movie class="col-xs-12 col-sm-12 col-md-6 col-lg-4"
        v-for="(item, index) in movies" :key="item.id"
        :id="item.id" :title="item.title"
        :cover="item.poster_path"
        :overview="item.overview" />
    </div>`,
    props: {
        movies: {
            type: Array,
            required: true,
            default: []
        }
    },
    components: {
        Movie
    }
});
