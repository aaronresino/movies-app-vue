let Movie = Vue.component('Movie', {
    template:
    `<div class="card p-0">
        <img :src="urlImage + cover" class="card-img-top"
             :alt="title">
        <div class="card-body">
            <h5 class="card-title" v-text="title"></h5>
            <p class="card-text">{{ overview | threeDots }}</p>
            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
        </div>
    </div>`,
    data() {
        return {
            urlImage: URL_IMAGE
        }
    },
    props: {
        id: {
            type: Number,
            required: true
        },
        title: {
            type: String,
            required: true
        },
        cover: {
            type: String,
            required: true
        },
        overview: {
            type: String,
            required: true
        }
    },
    filters: {
        threeDots (value) {
            return `${ value.substring(0, 40) }...`;
        }
    }
});
